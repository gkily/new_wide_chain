 $(function () {
    $(".left").css("height", $(".right").css("height"));
    $("#dm").css("background", "#314150");

    $(".copy").bind('click', function () {
        var Url2 = document.getElementById("txt_url"); //文本框内容
        Url2.select(); //选中内容
        document.execCommand("Copy"); //调用复制
        layer.alert("复制成功！");
    });
    
    
    $(".right .top .messages_all .messages_list span").click(function(e){
    	//var e = e || window.event;
		$(".messages_all .messages_list span").removeClass("active");
		$(this).addClass("active");
		$(this).children("sup").hide();
		$(".right .top .messages_all .messages_item").hide();
		$(".right .top .messages_all .messages_item").eq($(this).index()).show();
		//e.stopPropagation();
	})
	
	$(".right .top .messages_all .messages_item a").click(function(e){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
		}
	});
	$(".right .top .messages_all .messages_bottom span ").click(function(e){
		$(".right .top .messages_all .messages_item a").removeClass("active");
	})


	//判断:当前元素是否是被筛选元素的子元素或者本身
	jQuery.fn.isChildAndSelfOf = function(b){
	    return (this.closest(b).length > 0);
	};
	
	$(document).click(function(event){
		var event = event || window.event;
		//event.stopPropagation();
	    if($(event.target).isChildAndSelfOf(".messages_all")){
	    	$(".right .top .messages_all").show();
	    }else if ($(event.target).isChildAndSelfOf(".right .oper .message")){
	    	if($(".right .top .messages_all").css("display")=="block"){
	    		$(".right .top .messages_all").hide();			    		
	    	}else {
	    		$(".right .top .messages_all").show();			    		
	    	}
	    }else {
	    	$(".right .top .messages_all").hide();
	    }
	});
});

//var guanli = getClass("menu","dt");
/*for(var i=0;i<guanli.length;i++){
	guanli[i].onclick = function(){
		var dd = this.parentNode.getElementsByTagName("dd");
		var dis = getCss(dd[0],"display");
        if(dis == "block") {
			for(var j=0;j<dd.length;j++){
				dd[j].style.display = "none";
			}
		}else {
			for(var j=0;j<dd.length;j++){
				dd[j].style.display = "block";
			}
		}
	}
}*/

	
	/*消息*/
	$(".tabs li a").click(function(e){
		stopEvent(e);
		$(".tabs li a").removeClass('active');
		$(this).addClass('active');
		$('.data_table .tab').hide().eq($(this).parent().index()).show();
		$(".data_table .notice").hide().eq($(this).parent().index()).show();
	});


	/*tab*/
	$(".tabs li a").click(function(e){
		stopEvent(e);
		$(".tabs li a").removeClass('active');
		$(this).addClass('active');
		$('.data_table .tab').hide().eq($(this).parent().index()).show();
	})

	//我的广告 -- 删除
	$(".ads_del .check").click(function(e){
		stopEvent(e);
		if($(this).hasClass('checked')){
			$(this).removeClass('checked');
			$(this).text('点击选中');
		}else {
			$(this).addClass('checked');
			$(this).text('已选择');
		}
	});

	//删除按钮
	$(".ads_del .del").click(function(e){
		stopEvent(e);
		$(".ads_del .checked").parent().remove();
		$(".adsList .page .total").text("共"+$(".adsList .ads_ul li").length+"条");
	});

	$(".ads_del .checkAll").click(function(e){
		stopEvent(e);
		//alert($(this).html());
		if($(this).html() == "全选") {
			$(".ads_del .check").addClass("checked");
			$(".ads_del .check").text("已选中");
			$(this).text("取消");
		}else if($(this).html() == "取消"){
			$(this).text("全选");
			$(".ads_del .check").removeClass("checked");
			$(".ads_del .check").text("点击选中");
		}
	})

	//首页NAC
	$(".nac .nacSta").click(function(e){
		stopEvent(e);
		loadHtml("NAC.html");
	});

	//充值数量按钮
	$(".data_table .num a").click(function(e){
		stopEvent(e);
		$(".data_table .num a").removeClass('active');
		$(this).addClass('active');
	})


/*//给浏览器窗口的大小设置监听
$(window).resize(function(){
	var wW = document.body.clientWidth || document.documentElement.clientWidth;
	//alert(wW);
	if(wW<1350){
		$("#order").hide();
	}else {
		$("#order").show()
	}
})*/

	//个人信息修改
	//手机号
	$(".data_table .change .xiugai").click(function(){
		$(".modalLayer").show();
		$(".modalEmail").hide();
		$(".idenTel").show();
	});
	$(".modalLayer .idenTel .rele").click(function(){
		//alert($(this));
		$(".modalLayer .modalEmail").hide();
		$(".modalLayer .bindingTel").show()
	})

	//姓名
	$(".data_table .change .addName").click(function(){
		$("#username").show();
		//$("#username").focus();
		$(this).text("完成");
		/*$(this).click(function(){
			//$("#username").attr("disabled","disabled");
		})*/
	})

	//邮箱
	$(".data_table .change .modifyEmail").click(function(){
		$(".modalLayer").show();
		//alert($("#email").val());
		if($("#email").val()== "") {
			$(".bindingEmail").show()
		}else {
			$(".releEmail").show();
		}
		
	});
	
	//解绑按钮
	$(".modalLayer .releEmail .rele").click(function(e){
		stopEvent(e);
		$(".modalLayer .releEmail").hide();
		$(".bindingEmail").show();
	});


	//模态层关闭按钮
	$(".modalLayer .close").click(function(){
		$(".modalLayer").hide();
	})
	
	$(".page .list").click(function(){
		$(".page ul").show();
		$(".page ul li").click(function(){
			$(".page .pageNum input").val($(this).html());
			$(this).parent().hide();
		})
	});


	$(".save").click(function(){
		$(".container .note").show();
		setTimeout(function(){
			$(".container .note").hide();
		},3000);
		return false;
	})


/*广告分类和广告类型*/

//获取元素的类名
/*function getClass(oParent,sName){
    var obj = oParent.getElementsByTagName('*');
    var result = [];
    var re = eval('/\\b'+sName+'\\b/g');
    for(var i=0;i<obj.length;i++){
        if(re.test(obj[i].className)){
            result.push(obj[i]);
        }
    }
    return result;
}
*/

//function getClass(parent,clsName){
// //定义函数getByClass()实现获取document或指定父元素下所有class为on的元素  
//	var oParent=parent?document.getElementById(parent):document,
//		arr=new Array(),
//		cls=oParent.getElementsByTagName("*");
//	for(var i=0;i<cls.length;i++){
//		if(cls[i].className===clsName){/*其实用这种写法更优,应为一个元素可能有多个className,采用===判断符号无法解决这种情况,采用indexOf()可以判断出stringObject是否存在以及索引位置,如果是返回-1表示不存在.//cls[i].className.indexOf(clsName)!=-1*/
//			arr.push(cls[i]);
//		}
//	}
//	return arr;
//}

//获取样式
function getCss(curEle,attr){
    var val = null;
    if(/MSIE(6|7|8)/.test(navigator.userAgent)){
        val = curEle.currentStyle[attr];
    }else{
        val = window.getComputedStyle(curEle,null)[attr];
    }
    return val;
}

////点击右侧加载
//function loadHtml(url){
//	$("#right").load(url);
//}

function stopEvent(e){
	var e=e||window.event;
    e.stopPropagation();//阻止事件的传播(包括捕获也包括冒泡)

    // 兼容处理
    if (e.stopPropagation) {
        e.stopPropagation();
    }else{
        // IE浏览器
        e.cancelBubble=true;
    }

}


	$(".adsList .page .total").text("共"+$(".adsList .ads_ul li").length+"条");
	/*if($("#email").val()==""){
		$("#email").hide();
	}else {
		$("#email").show()
	}*/
	
	

//	$(".messages_all .messages_list span").click(function(){
//		$(".messages_all .messages_list span").removeClass("active");
//		$(this).addClass("active");
//		$(this).children("sup").hide();
//		$(".right .top .messages_all .messages_item").hide();
//		$(".right .top .messages_all .messages_item").eq($(this).index()).show();			
//	})

	


